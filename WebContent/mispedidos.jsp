<%@ page contentType="text/html; charset=UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<h3><s:text name="titulo_mispedidos" /></h3>
<p><s:text name="texto_mispedidos" /></p>
<div id="pedidos">
	<s:if test="%{pedidosSize==0}">
		<p class="warning"><s:text name="no_hay_pedidos" /></p>
	</s:if>
	<s:else>	
		<s:iterator value="pedidos"	var="pedido">
			<div class="pedido">
				<div class="head"><s:text name="fecha_pedido" /> <b><s:property value="#pedido.moment"/></b></div>
				<table class="light_rows">
					<tbody>
						<s:iterator value="#pedido.items" var="items" status="itStatus">
							<tr>
								<td class='desc'>
									<div class="nombre"><s:property value="#items.nombre"/></div>
								</td>
								<td class='precio'>
									$<s:property value="#items.precio"/>
								</td>
								<td class='cantidad'>
									<s:property value="#items.cantidad"/>
								</td>
							</tr>
						</s:iterator>
					</tbody>
					<tfoot>
						<tr class="total">
							<td>Total</td><td>$<s:property value="#pedido.total"/></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</s:iterator>
	</s:else>
</div>
