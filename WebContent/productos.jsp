<%@ page contentType="text/html; charset=UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<s:if test="%{#session.logged}">
	<script type="text/javascript">
		var error = false;
		$.subscribe('errorState', function(event,data) {
			error = true;
		});
		$.subscribe('complete', function(event,data) {
			if (error) { // no hay que sumar
				error = false;
			} else {
				var count = parseInt($.trim($('#count').text()));
				$('#count').text(++count);
			}
		});
	</script>
</s:if>

<table id="#productos" class="light_rows">
	<tbody>
		<s:iterator value="productos" status="itStatus">
			<tr>
				<td class='desc'>
					<h2 class="nombre"><s:property value="nombre"/></h2>
	  				<div class="descripcion"><s:property value="descripcion"/></div>
				</td>
				<td class='precio'>
					<s:if test="%{#session.logged}">
						$<s:property value="precio"/>
					</s:if>
					<s:else>
						<s:text name="login_ver_precio" />
					</s:else>
				</td>
				<s:if test="%{#session.logged}">
					<td>
						<s:form id="form_%{#itStatus.count}" action="doadd" validate="true">
							<input type="hidden" name="productoId" value="<s:property value='id'/>" />
							<input type="hidden" name="cantidad" value="1" />
							<sj:submit value="+" indicator="indicator"  button="true" targets="result" 
									onBeforeTopics="before" 
					            	onCompleteTopics="complete" 
					            	onErrorTopics="errorState" />
						</s:form>
					</td>
				</s:if>
			</tr>
		</s:iterator>
	</tbody>
</table>