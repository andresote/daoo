<%@ page contentType="text/html; charset=UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:useBean id="currentPage" type="java.lang.String" scope="request"/>

<div id="menu">
	<s:if test="%{#request.currentPage=='productos'}">
		<a href="productos.action" class="selected">Productos</a>
	</s:if>
	<s:else>
		<a href="productos.action">Productos</a>
	</s:else>
	
	<s:if test="%{#session.logged}">
		<s:a href="dologout">Cerrar sesión</s:a>
		<div id="user-info">
			<a href="mispedidos">Mis pedidos</a>
			<a href="carrito" class="carrito_wrapper">
				<img id="cart-img" src="img/cart.png" />
				(<span id="count">
					<s:if test="%{#session.carritocount}">
						<s:property value="%{#session.carritocount}"/>
					</s:if>
					<s:else>
						0
					</s:else>
				</span>)
			</a>
		</div>
	</s:if>
	<s:else>
	
		<s:if test="%{#request.currentPage=='login'}">
			<a href="login" class="selected">Acceder</a>
		</s:if>
		<s:else>
			<a href="login">Acceder</a>
		</s:else>
	</s:else>
</div>