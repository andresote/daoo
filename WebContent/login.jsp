<%@ page contentType="text/html; charset=UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<p><s:text name="texto_login" /></p>
<s:actionerror />
<s:form action="dologin" method="post">

	<s:textfield name="name" key="label.name" size="20" />
	<s:password name="pass" key="label.pass" size="20" />

	<s:submit method="execute" key="label.login" align="center" />
</s:form>
