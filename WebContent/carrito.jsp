<%@ page contentType="text/html; charset=UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<h3><s:text name="titulo_carrito" /></h3>
<p><s:text name="texto_carrito" /></p>
<form action='doconfirm' method='post'>
	<table id="carrito" class="light_rows">
		<tbody>
			<s:iterator value="productos" status="itStatus">
				<tr>
					<td class='desc'>
						<div class="nombre"><s:property value="nombre"/></div>
					</td>
					<td class='precio'>
						$<s:property value="precio"/>
					</td>
					<td class='cantidad'>
						<input 	type="text" name="carrito.items[<s:property value="#itStatus.index"/>].cantidad" 
								value='<s:property value="cantidad"/>' />
					</td>
				</tr>
			</s:iterator>
		</tbody>
		<tfoot>
			<tr class="total">
				<td>Total</td><td>$<span id="total_carrito"><s:property value="total"/></span></td>
				<script type="text/javascript">
					$("#carrito .cantidad input").keyup(function() {
						var total = 0;
						$('#carrito tbody tr').each(function() {
							var precio = parseFloat($.trim($(this).find('.precio').text()).substring(1));
							var cant = parseInt($(this).find('.cantidad input').val());
							
							if (! isNaN(precio) && ! isNaN(cant)) {
								total += precio * cant;	
							}
						});
						$("#total_carrito").text(total);
					});
				</script>
			</tr>
		</tfoot>
	</table>
	<button type="submit"><s:text name="confirmar" /></button>
</form>