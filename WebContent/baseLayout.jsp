<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html lang='es' xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<sj:head locale="es" jqueryui="true" defaultIndicator="indicator" debug="true" compressed="false"/>
		<title><tiles:insertAttribute name="title" ignore="true" /></title>
		<link href="css/estilo.css" rel="stylesheet"/>
	</head>
	
	<body>
		<tiles:useAttribute name="currentPage" scope="request" />
		
		<img id="indicator" src="img/loading.gif" alt="Cargando..." style="display:none"/>
		<div id="header">
			<tiles:insertAttribute name="header" />
		</div>
		
		<div id="menu">
			<tiles:insertAttribute name="menu" />
		</div>
		
		<div id="content">
			<tiles:insertAttribute name="body" />
		</div>
		
		<div id="footer">
			<tiles:insertAttribute name="footer" />
		</div>
	</body>
</html>
