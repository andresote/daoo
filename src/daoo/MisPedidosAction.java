package daoo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;

import daoo.modelo.Dao;
import daoo.modelo.Pedido;

public class MisPedidosAction {
	protected ArrayList<Pedido> pedidosCache = null;
	
	public ArrayList<Pedido> getPedidos() {
		if (this.pedidosCache != null) {
			return this.pedidosCache;
		}
		
		ResultSet pedidos = null;
		ArrayList<Pedido> rs = new ArrayList<Pedido>();
		
		try {
			Map<String, Object> session = ActionContext.getContext().getSession();
			Object u = session.get("userid");
			pedidos = Dao.instancia().getPedidos(((Integer) u).intValue());

			while (pedidos.next()) {
				int pedidoId = pedidos.getInt(1);
				String moment = pedidos.getString(2);
				rs.add(new Pedido(Dao.instancia().getPedido(pedidoId), moment));
			}
			
			pedidos.close();	
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		this.pedidosCache = rs;
		return rs;
	}
	
	public int getPedidosSize() {
		return this.getPedidos().size();
	}
	
	public String execute() {
		return "success";
	}
}
