package daoo.modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;


public class Carrito {
	private static Carrito instancia;
	
	public static Carrito instancia() {
		if (instancia == null) {
			instancia = new Carrito();
		}
		
		return instancia;
	}
	
	private Carrito() {	}
	
	private static final String SESSKEY = "carrito";
	private static final String SESSCOUNTKEY = "carritocount";
	
	private ArrayList<CarritoItem> items;
	
	private Map<String, Object> getSession() {
		return ActionContext.getContext().getSession();
	}
	
	private ArrayList<CarritoItem> getFromSession() {
		Map<String, Object> session = this.getSession();
		ArrayList<CarritoItem> carrito = null;
		if (session.containsKey(SESSKEY)) {
			carrito = (ArrayList<CarritoItem>) session.get(SESSKEY);
		} else {
			carrito = new ArrayList<CarritoItem>();
		}
		
		return carrito;
	}
	
	private void save() {
		this.getSession().put(SESSKEY, this.items);
		this.getSession().put(SESSCOUNTKEY, this.items.size());
	}
	
	public int size() {
		Map<String, Object> session = this.getSession();
		if (session.containsKey(SESSCOUNTKEY)) {
			return ((Integer) session.get(SESSCOUNTKEY)).intValue();
		}
		
		return 0;
	}
	
	public void vaciar() {
		this.items = new ArrayList<CarritoItem>();
		this.save();
	}
	
	public ArrayList<CarritoItem> getItems() {
		if (this.items == null) {
			this.items = this.getFromSession();
		}
		
		return this.items;
	}
	
	public boolean contiene(CarritoItem i) {
		return this.getItems().contains(i);
	}
	
	public void addItem(CarritoItem i) {
		ArrayList<CarritoItem> items = this.getItems();
		items.add(i);
		this.save();
	}
	
	public void removeItem(CarritoItem i) {
		ArrayList<CarritoItem> items = this.getItems();
		items.remove(i);
		this.save();
	}
	
	/**
	 * Trae los items del carrito cargados con data extra (el nombre, descripción y precio)
	 * @return
	 */
	public ArrayList<CarritoItem> getItemsWithExtra() {
		ArrayList<CarritoItem> carrito = this.getItems();
		
		ArrayList<Integer> ids = new ArrayList<Integer>(); 
		for (CarritoItem i : carrito) {
			ids.add(i.getProductoId());
		}
		
		try {
			ResultSet rs = Dao.instancia().getProductosFromIds(ids);

			while (rs.next()) {
				// se actualizan los valores de los items en el carrito
				for (CarritoItem i : carrito) {
					if (i.getProductoId() == rs.getInt("producto_id")) {
						i.setNombre(rs.getString("nombre"));
						i.setDesc("descripcion");
						i.setPrecio(rs.getFloat("precio"));
						break;
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return carrito;
	}
	
	/**
	 * guarda el contenido del carrito como un pedido en la base
	 * y reinicia el carrito para poder armar un nuevo pedido
	 */
	public void saveToDb(int usuarioId) throws SQLException {
		// se escribe en la base
		this.dbTransaction(usuarioId);
		
		// se vacia el carrito
		this.vaciar();
	}
	
	private void dbTransaction(int usuarioId) throws SQLException {
		Db.instancia().getConn().setAutoCommit(false);
		
		// Se crea la entrada del pedido
		String sql = "INSERT INTO compra (compra_id, usuario) VALUES (NULL, " + usuarioId + ")";		
		PreparedStatement st = Db.instancia().preparar(sql);
		st.executeUpdate();
		int compra = this.getLastCompraId(st);
		
		// Se guardan las líneas del pedido
		sql = "INSERT INTO compra_producto (compra, producto, precio, cantidad) VALUES (?, ?, ?, ?)";
		st = Db.instancia().preparar(sql);

		for (CarritoItem i : this.getItems()) {
			st.setInt(1, compra);
			st.setInt(2, i.getProductoId());
			st.setFloat(3, i.getPrecio());
			st.setInt(4, i.getCantidad());
			st.executeUpdate();
		}

		Db.instancia().getConn().commit();
	}
	
	private int getLastCompraId(PreparedStatement st) throws SQLException {
		ResultSet rs = st.getGeneratedKeys();
		rs.next();
		int id = rs.getInt(1);
		rs.close();
		return id;
	}
	
	public float getTotal() {
		float total = 0;
		
		ArrayList<CarritoItem> carrito = this.getItems();
		
		for (CarritoItem i : carrito) {
			total += i.getPrecio() * i.getCantidad();
		}
		
		return total;
	}
}
