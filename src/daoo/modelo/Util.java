package daoo.modelo;

import java.util.ArrayList;

public class Util {
	static public <T> String implode(ArrayList<T> list, String glue) {
		String output = "";
		String glueAux = "";
		
		StringBuilder buffer = new StringBuilder();
		for (T item : list) {
			buffer.append(glueAux);
			glueAux = glue;
			buffer.append(item.toString());
		}
		
		output = buffer.toString();
		return output;
	}
}
