package daoo.modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Pedido {
	private ArrayList<CarritoItem> items;
	private String moment;
	
	public Pedido(ResultSet rs, String moment) throws SQLException {
		this.moment = moment;
		
		this.items = new ArrayList<CarritoItem>();
		
		while (rs.next()) {
			CarritoItem i = new CarritoItem(rs.getInt(1));
			i.setNombre(rs.getString(2));
			i.setPrecio(rs.getFloat(3));
			i.setCantidad(rs.getInt(4));
			this.items.add(i);
		}
		
		rs.close();
	}
	
	public ArrayList<CarritoItem> getItems() {
		return this.items;
	}
	
	public float getTotal() {
		float total = 0;
		for (CarritoItem i : this.items) {
			total += i.getPrecio() * i.getCantidad();
		}
		
		return total;
	}
	
	public String getMoment() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
		    Date today = df.parse(this.moment);
		    return new SimpleDateFormat("dd/MM/yyyy 'hora:' HH:mm").format(today);
		} catch (ParseException e) {
		   e.printStackTrace();
		}
		
		return "";

	}
}
