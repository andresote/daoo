package daoo.modelo;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class Db {
	private static Db instancia;
	
	public static Db instancia() {
		if (instancia == null) {
			instancia = new Db();
		}
		
		return instancia;
	}
	
	protected Connection conn = null;
	
	private Db() {
	}
	
	protected void crear() throws Exception {
		Context initCtx = new InitialContext();
		DataSource ds = (DataSource) initCtx.lookup("java:comp/env/jdbc/daoo");
		
		// Allocate and use a connection from the pool
		Connection conn = ds.getConnection();
		
		this.conn = conn;
	}
	
	public Connection getConn() throws SQLException {
		if (this.conn == null || this.conn.isClosed()) {
			try {
				this.crear();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return this.conn;
	}
	
	public void cerrar() throws SQLException {
		if (this.conn != null && ! this.conn.isClosed()) {
			this.conn.close();	
		}
	}
	
	public PreparedStatement preparar(String sql) throws SQLException {
		return this.getConn().prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
	}
}
