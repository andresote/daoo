package daoo.modelo;

import java.security.MessageDigest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Dao {
	private static Dao instancia;
	
	public static Dao instancia() {
		if (instancia == null) {
			instancia = new Dao();
		}
		
		return instancia;
	}
	
	private Dao() {}
	
	public Integer authUser(String name, String pass) throws SQLException {
//		String hashedPass = MessageDigest.getInstance("MD5").digest(pass.getBytes("UTF-8")).toString();
		String hashedPass = pass;
		
		String sql = "SELECT usuario_id FROM usuario WHERE nombre = ? AND pass = ?";
		PreparedStatement st = Db.instancia().preparar(sql);
		st.setString(1, name);
		st.setString(2, hashedPass);
		ResultSet rs = st.executeQuery();
		if (rs.next()) {
			return rs.getInt(1);
		}
		
		return null;
	}
	
	public ResultSet getProductosFromIds(ArrayList<Integer> ids) throws SQLException {
		ArrayList<String> params = new ArrayList<String>();
		for (int i = 0; i < ids.size(); i++) {
			params.add("?");
		}

		String sql = "SELECT * FROM producto WHERE producto_id IN (" + Util.implode(params, ",") + ")";
		PreparedStatement st = Db.instancia().preparar(sql);

		for (int j = 1; j <= ids.size(); j++) {
			st.setString(j, ids.get(j - 1).toString());
		}
		
		return st.executeQuery();
	}
	
	public ResultSet getProductos() throws SQLException {
		return this.getProductos("");
	}
	
	public ResultSet getProductos(String query) throws SQLException {
		String sql = "SELECT * FROM producto WHERE nombre LIKE ?";
		PreparedStatement st = Db.instancia().preparar(sql);
		st.setString(1, "%" + query + "%");
		return st.executeQuery();
	}
	
	public ResultSet getPedidos(int usuarioId) throws SQLException {
		String sql = "SELECT compra_id, moment FROM compra WHERE usuario = ? ORDER BY moment DESC";
		PreparedStatement st = Db.instancia().preparar(sql);
		st.setInt(1, usuarioId);
		return st.executeQuery();
	}
	
	public ResultSet getPedido(int pedidoId) throws SQLException {
		String sql = "" +
				"SELECT cp.producto, p.nombre, cp.precio, cp.cantidad " +
				"FROM compra_producto cp, producto p " +
				"WHERE cp.producto=p.producto_id AND compra = ?";
		PreparedStatement st = Db.instancia().preparar(sql);
		st.setInt(1, pedidoId);
		return st.executeQuery();
	}
}
