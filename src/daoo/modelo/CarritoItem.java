package daoo.modelo;

import java.io.Serializable;

/**
 * Clase que representa un item en el carrito
 */
public class CarritoItem implements Serializable {
	private int productoId;
	private int cantidad;
	
	// estas 3 propiedades se usan sólo en la vista carrito
	private String nombre;
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	private String desc;
	private float precio;
	
	public CarritoItem(int productoId) {
		this(productoId, 0);
	}
	
	public CarritoItem(int productoId, int cantidad) {
		this.productoId = productoId;
		this.cantidad = cantidad;
	}
	
	public int getProductoId() {
		return productoId;
	}
		
	public int getCantidad() {
		return cantidad;
	}
	
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	@Override
	public String toString() {
		return "daoo.modelo.CarritoItem@id:" + this.productoId + "|cantidad:" + this.cantidad;
	}
	
	@Override
	public boolean equals(Object i) {
		return getProductoId() == ((CarritoItem) i).getProductoId();
	} 
	
	@Override
	public int hashCode() {
		return getProductoId();
	}
}