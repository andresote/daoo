package daoo.modelo;

public class Producto {
	private int id;
	private String nombre;
	private float precio;
	private String descripcion;
	
	public Producto(int id, String nombre, float precio, String descripcion) {
		this.id	= id;
		this.nombre = nombre;
		this.precio = precio;
		this.descripcion = descripcion;
	}
	
	public int getId() {
		return id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public float getPrecio() {
		return precio;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
}
