package daoo;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import daoo.modelo.Dao;
import daoo.modelo.Db;

public class LoginAction extends ActionSupport {
	private String name;
	private String pass;

	public LoginAction() {
		this.name = "";
		this.pass = "";
	}
	
	public String getName() {
		return this.name;
	}

	public String getPass() {
		return this.pass;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
	public String execute() {
		try {
			Integer id = Dao.instancia().authUser(this.getName(), this.getPass()); 
			if (id != null) {
				Map<String, Object> session = ActionContext.getContext().getSession();
				session.put("logged", true);
				session.put("username", this.getName());
				session.put("userid", id.intValue());
				return "success";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "fail";
	}
	public String logout() throws Exception {
		Map<String, Object> session = ActionContext.getContext().getSession();
		session.clear();
		return "success";
	}
}
