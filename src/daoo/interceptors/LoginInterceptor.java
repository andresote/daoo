package daoo.interceptors;

import java.util.Map;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class LoginInterceptor implements Interceptor {
	@Override
	public void destroy() {}

	@Override
	public void init() {}

	@Override
	public String intercept(ActionInvocation inv) throws Exception {
		Map<String, Object> session = inv.getInvocationContext().getSession();
		Object u = session.get("username");
		
		if (u == null) {
			return "login";
		} else {
			return inv.invoke();
		}
	}

}
