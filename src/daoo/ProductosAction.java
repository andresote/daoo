package daoo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import daoo.modelo.Dao;
import daoo.modelo.Producto;

public class ProductosAction {
	private ArrayList<Producto> productos;

	public String execute() {
		ResultSet rs;
		try {
			rs = Dao.instancia().getProductos();
			this.productos = new ArrayList<Producto>();
			while (rs.next()) {
				Producto p = new Producto(
					rs.getInt("producto_id"), 
					rs.getString("nombre"), 
					rs.getFloat("precio"), 
					rs.getString("descripcion")
				);
				
				this.productos.add(p);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "fail";
		}
		
		return "success";
	}

	public ArrayList<Producto> getProductos() {
		return productos;
	}
}
