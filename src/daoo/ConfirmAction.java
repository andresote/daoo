package daoo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;

import daoo.modelo.Carrito;

public class ConfirmAction {
	
	public Carrito getCarrito() {
		return Carrito.instancia();
	}

	public String confirm() {
		try {
			Map<String, Object> session = ActionContext.getContext().getSession();
			Object u = session.get("userid");
			Carrito.instancia().saveToDb(((Integer) u).intValue());
			return "success";
		} catch (SQLException e) {
			e.printStackTrace();
			return "error";
		}
	}
}
