package daoo;

import java.util.ArrayList;

import com.opensymphony.xwork2.ActionSupport;

import daoo.modelo.Carrito;
import daoo.modelo.CarritoItem;

public class CartAction extends ActionSupport {
	// Salidas posibles de la acción
	public static final String OUT_SUCCESS  = "success";
	public static final String OUT_REPEATED = "repeated";
	public static final String OUT_NOTFOUND = "notfound";
	
	public String execute() {
		// La vista del carrito
		return OUT_SUCCESS;
	}
	
	/**
	 * Agrega un elemento al carrito
	 * @return
	 */
	public String add() {
		CarritoItem i = new CarritoItem(this.getProductoId(), this.getCantidad());
		
		Carrito c = Carrito.instancia();
		
		if (c.contiene(i)) {
			return OUT_REPEATED;
		}
		
		c.addItem(i);
		return OUT_SUCCESS;
	}

	/**
	 * Saca un elemento al carrito
	 * @return
	 */
	public String remove() {
		CarritoItem i = new CarritoItem(this.getProductoId());
		
		Carrito c = Carrito.instancia();
		
		if (c.contiene(i)) {
			return OUT_NOTFOUND;
		}
		
		c.removeItem(i);
		return OUT_SUCCESS;
	}
	
	private int productoId;
	private int cantidad;
	
	public CartAction() {
	}
	
	public int getTotalProductos() {
		return Carrito.instancia().size();
	}

	public int getProductoId() {
		return productoId;
	}
	
	public void setProductoId(int productoId) {
		this.productoId = productoId;
	}
	
	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	public ArrayList<CarritoItem> getProductos() {
		return Carrito.instancia().getItemsWithExtra();
	}
	
	public float getTotal() {
		return Carrito.instancia().getTotal();
	}
}
