BEGIN TRANSACTION;

-- Usuarios
INSERT INTO usuario (nombre, pass) VALUES ('pepe', 'pepe');
INSERT INTO usuario (nombre, pass) VALUES ('juan', 'juan');
INSERT INTO usuario (nombre, pass) VALUES ('admin', 'admin');

-- Productos
INSERT INTO producto (nombre, precio, descripcion) 
	VALUES ('Mouse Genius', 3.14, 'Mouse óptico Genius Xscroll Usb 800 Dpi Win7 Mac Microcentro');
INSERT INTO producto (nombre, precio, descripcion) 
	VALUES ('Sintetizador Korg', 4299, 'Sintetizador Korg X50');
INSERT INTO producto (nombre, precio, descripcion) VALUES ('Bolsa de dormir', 729, 'Bolsa de dormir Waterdog Everest 300');
INSERT INTO producto (nombre, precio, descripcion) VALUES ('Ipad 2', 3700, '16gb Wifi Camara Hd Dual Core A5 Ipad2 Apple');
INSERT INTO producto (nombre, precio, descripcion) VALUES ('Samsung Galaxy S2', 3689.70, 'I9100 3g Android 2.3 Dual Core 1.2ghz 8mpx');
	
COMMIT TRANSACTION;