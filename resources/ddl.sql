BEGIN TRANSACTION;

DROP TABLE IF EXISTS usuario;
CREATE TABLE usuario (
	usuario_id	INTEGER PRIMARY KEY,
	nombre	TEXT NOT NULL,
	pass	TEXT NOT NULL,
	moment	DATE DEFAULT CURRENT_TIMESTAMP
);

DROP TABLE IF EXISTS producto;
CREATE TABLE producto (
	producto_id INTEGER PRIMARY KEY,
	nombre TEXT NOT NULL,
	precio REAL NOT NULL,
	descripcion TEXT DEFAULT ''
);

DROP TABLE IF EXISTS compra;
CREATE TABLE compra (
	compra_id INTEGER PRIMARY KEY,
	usuario INTEGER,
	moment	DATE DEFAULT CURRENT_TIMESTAMP,
	
	FOREIGN KEY (usuario) REFERENCES usuario(usuario_id)
);

DROP TABLE IF EXISTS compra_producto;
CREATE TABLE compra_producto (
	compra INTEGER,
	producto INTEGER,
	precio REAL,
	cantidad INTEGER DEFAULT 1,
	
	FOREIGN KEY (compra) REFERENCES compra(compra_id),
	FOREIGN KEY (producto) REFERENCES producto(producto_id)
);

COMMIT TRANSACTION;